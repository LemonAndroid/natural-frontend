import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  infinity: service(),

  model() {
    let table = this.modelFor('table');
    return Ember.RSVP.hash({
      table: table,
      rows: this.infinity.model('row', {
        perPage: 12,
        startingPage: 1,
        table_id: table.id,
        totalPagesParam: 'meta.total-pages'
       })
    });
  }
});
